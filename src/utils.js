const clientID = 'H1Q3BCX1OJ3242HOW1XXAUVEUEUBGMHDIV5PLSUYB4EIRKUV';
const clientSecret = 'EGBXRXO0RGJFVKFIIQENG4A0SN3Q15Z1WIG01OUTITSXEYGH';
const redirectUri = 'https://wowpizza.herokuapp.com/login-auth';
const loginUrl = `https://foursquare.com/oauth2/authenticate?client_id=${clientID}&response_type=token&redirect_uri=${redirectUri}`;

const getUserLocation = () => {
    if (navigator.geolocation) {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resolve, reject);
        });
    }
    else {
        return {
            type: 'error',
            payload: 'Geolocation not supported!'
        };
    }
};

const getUrlToken = (name) => {
    const hash = window.location.hash;
    return hash.indexOf('oauth_token') ? hash.substring( hash.indexOf('=') + 1 ) : false;
}

export {
    loginUrl,
    clientID,
    clientSecret,
    redirectUri,
    getUrlToken,
    getUserLocation
}