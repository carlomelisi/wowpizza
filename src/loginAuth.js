import React, { Component } from 'react';
import { getUrlToken } from './utils';
import { connect } from 'react-redux';
import { setAuthToken } from './actions';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router'

class LoginAuth extends Component {
  setToken(token) {
    Cookies.set('oauth_token', token, { expires: 14 });
  }

  componentDidMount() {
    const accessToken = getUrlToken();
    if (accessToken) {
      this.setToken(accessToken);
      this.props.setAuthToken(accessToken);
    }
  }
  render() {
    return <div>Login auth</div>;
  }
}

const mapStateToProps = state => {
    return {
      feed: state.feed,
      feedIsLoading: state.feedIsLoading,
      locationError: state.locationError
    };
  };

  const mapDispatchToProps = dispatch => ({
    setAuthToken: authToken => dispatch(setAuthToken(authToken))
  })

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginAuth));