import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { loginUrl } from './utils';
import pizza from './pizza.svg';

import './css/login.css';

class Login extends Component {
  render() {
    return <div className="login">
      <img className="login__image" src={pizza} alt="pizza" />
      <h1 className="login__title">WOWPIZZA</h1>
      <h2 className="login__subtitle">A place where you can find the nearest pizza. Mamma mia!</h2>
      <a href={loginUrl} className="btn btn-primary">Ok show me that <span role="img" aria-label="pizza icon">🍕</span></a>
    </div>;
  }
}

const mapStateToProps = state => {
  return {
    feed: state.feed,
    feedIsLoading: state.feedIsLoading,
    locationError: state.locationError
  };
};

const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));