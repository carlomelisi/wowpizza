const defaultState = {
    feed: [],
    feedIsLoading: true
};

const reducers = (state = defaultState, action) => {
    switch (action.type) {
        case 'SET_FEED':
            return {
                ...state,
                feedIsLoading: false,
                feed: action.feed
            };
        case 'FEED_LOADiNG':
            return {
                ...state,
                feedIsLoading: true
            };
        case 'SET_LOCATION_ERROR':
            return {
                ...state,
                locationError: action.error.message
            };
        case 'SET_AUTH_TOKEN':
            return {
                ...state,
                authToken: action.authToken
            };
        default:
            return state;
    }
}

export default reducers;