export const setFeed = feed => ({
    type: 'SET_FEED',
    feed
});

export const feedLoading = () => ({
    type: 'FEED_LOADING'
});

export const setLocationError = error => ({
    type: 'SET_LOCATION_ERROR',
    error
});

export const setAuthToken = authToken => ({
    type: 'SET_AUTH_TOKEN',
    authToken
});