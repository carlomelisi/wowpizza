import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setFeed, feedLoading, setLocationError } from './actions';
import { withRouter } from 'react-router'
import { getUserLocation } from './utils';
import Cookies from 'js-cookie';
import axios from 'axios';

import pizza from './pizza.svg';
import './css/Feed.css';

class Feed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coords: false
    }
  }
  getVenueRecommendation(coords) {
    this.props.feedLoading();
    return axios.get('https://api.foursquare.com/v2/venues/explore', {
        params: {
          oauth_token: Cookies.get('oauth_token'),
          v: '20170803',
          venuePhotos: 1,
          openNow: 1,
          sortByDistance: 1,
          limit: 10,
          query: 'pizza',
          ll: `${coords.latitude},${coords.longitude}`
        }
    });
  }

  componentDidMount() {
    const userLocation = getUserLocation();
    userLocation.then(result => {
      this.setState({coords: result.coords});
      this.getVenueRecommendation(result.coords).then(response => {
        this.props.setFeed(response.data.response.groups[0].items);
      });
    })
    .catch(error => this.props.setLocationError(error));
  }
  render() {
    const { feed, feedIsLoading, locationError } = this.props;

    if (locationError) {
      return <div className="feed-error">{locationError}</div>;
    }

    if (feedIsLoading) {
      return <div className="feed-loading">Feed loading...</div>;
    }

    return (
      <div className="feed">
        <ul className="feed__list">
          {feed.map(item => <li key={item.venue.id} className="feed__list-item">
            <img className="feed__list-item__image" src={pizza} alt="pizza icon" />
            <div className="feed__list-item__content">
              <h3 className="feed__list-item__title">{item.venue.name}</h3>
              <h4 className="feed__list-item__subtitle">{item.venue.location.distance}m ⋅
                <a href={`https://www.google.com/maps/dir/?api=1&origin=${this.state.coords.latitude},${this.state.coords.longitude}&destination=${item.venue.name},${item.venue.location.address},${item.venue.location.city}&travelmode=walking`} className="feed__list-item__link" target="_blank" rel="nofollow external">Take me there</a>
              </h4>
            </div>
          </li>)}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    feed: state.feed,
    feedIsLoading: state.feedIsLoading,
    locationError: state.locationError,
    authToken: state.authToken
  };
};

const mapDispatchToProps = dispatch => ({
  setFeed: feed => dispatch(setFeed(feed)),
  feedLoading: feed => dispatch(feedLoading(feed)),
  setLocationError: error => dispatch(setLocationError(error)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Feed));
