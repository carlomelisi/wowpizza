import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { setAuthToken } from './actions';
import Cookies from 'js-cookie';

class Logout extends Component {
  componentWillMount() {
    Cookies.remove('oauth_token');
    this.props.setAuthToken(false);
  }
  render() {
    return <div className="logout">Logging out</div>;
  }
}

const mapStateToProps = state => {
  return {
    feed: state.feed,
    feedIsLoading: state.feedIsLoading,
    locationError: state.locationError
  };
};

const mapDispatchToProps = dispatch => ({
  setAuthToken: authToken => dispatch(setAuthToken(authToken))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Logout));