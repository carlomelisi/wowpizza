import React, { Component } from 'react';
import { loginUrl } from './utils';
import { Route, Switch, Redirect, Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { connect } from 'react-redux';
import { setAuthToken } from './actions';
import Cookies from 'js-cookie';

import LoginAuth from './loginAuth';
import Login from './login';
import Logout from './logout';
import Feed from './feed';
import './css/App.css';

class App extends Component {
  componentWillMount() {
    const authToken = Cookies.get('oauth_token');
    if (authToken) {
      this.props.setAuthToken(authToken);
    }
  }

  render() {
    const authToken = this.props.authToken;

    return (
      <div className="app">
        <div className="app__container">
          <header>
            <div className="wrap wrap--header">
              <Link to="/" className="logo">
                <span className="logo__wow">WOW</span>PIZZA</Link>
              <nav className="menu">
              {!this.props.authToken && <a href={loginUrl}>Login</a>}
              {this.props.authToken && <Link to="/logout">Logout</Link>}
              </nav>
            </div>
          </header>
          <div className="wrap">
            <Switch>
              <Route exact path='/' render={() => {
                if (authToken) {
                  return <Redirect to='/feed'/>;
                }
                return <Login />;
              }} />
              <Route exact path='/logout' render={() => {
                if (!authToken) {
                  return <Redirect to='/'/>;
                }
                return <Logout />;
              }} />
              <Route path='/feed' render={() => {
                if (!authToken) {
                  return <Redirect to='/'/>;
                }
                return <Feed />;
              }} />
              <Route path='/login-auth' render={() => {
                if (authToken) {
                  return <Redirect to='/feed'/>;
                }
                return <LoginAuth />;
              }} />
            </Switch>
          </div>
        </div>
        <footer>
          <div className="wrap">
            Made with <span role="img" aria-label="heart icon">❤️</span> by Ninja Turtles
          </div>
        </footer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    feed: state.feed,
    feedIsLoading: state.feedIsLoading,
    locationError: state.locationError,
    authToken: state.authToken
  };
};

const mapDispatchToProps = dispatch => ({
  setAuthToken: authToken => dispatch(setAuthToken(authToken))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
